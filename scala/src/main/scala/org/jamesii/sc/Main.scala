package org.jamesii.sc

import org.jamesii.sc.sim.Simulator

/**
  * @author Tom Warnke
  */
object Main extends App {

  val model = new Model {
    operation ("op1") (0.4, 0.1)
    test ("op1-test") (0.05) scrap (0.5) rework (0.1, 0.05)
    operation ("op2") (0.4, 0.1)
    test ("op2-test") (0.05) scrap (0.5) rework (0.1, 0.05)
    operation ("op3") (0.4, 0.1)
    test ("op3-test") (0.05) scrap (0.5) rework (0.1, 0.05)
    operation ("op4") (0.4, 0.1)
    test ("op4-test") (0.05) scrap (0.5) rework (0.1, 0.05)
    operation ("op5") (0.4, 0.1)
    test ("op5-test") (0.05) scrap (0.5) rework (0.1, 0.05)
  }

  val times = Simulator.execute(model, 100)

  println(s"\n\nAverage lead-time: ${times.sum / times.size}")

}
