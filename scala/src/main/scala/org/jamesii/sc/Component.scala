package org.jamesii.sc

import org.apache.commons.math3.distribution.{LogNormalDistribution, UniformRealDistribution}
import org.jamesii.sc.sim.Item

import scala.collection.mutable

/**
  * @author Tom Warnke
  */
abstract class Component {
  val inputBuffer = new mutable.Queue[Item]()
}

case class Operation(name: String, location: Double, shape: Double) extends Component {

  val durationDistribution = new LogNormalDistribution(location, shape)

  def duration() = durationDistribution.sample()

}

case class Test(name: String, failureProb: Double, scrapProb: Double, location: Double, shape: Double) extends Component {

  val unif = new UniformRealDistribution()

  val durationDistribution = new LogNormalDistribution(location, shape)

  def isFailure(): Boolean = unif.sample() < failureProb

  def isScrap(): Boolean = unif.sample() < scrapProb

  def duration() = durationDistribution.sample()

}