package org.jamesii.sc.sim

import org.jamesii.sc._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * @author Tom Warnke
  */
object Simulator {

  implicit val ord = new Ordering[Event] {
    override def compare(x: Event, y: Event): Int = -x.time.compare(y.time)
  }

  object Sink extends Component {
    val times: ListBuffer[Double] = ListBuffer.empty[Double]
  }

  val schedule = new mutable.PriorityQueue[Event]()
  var currentTime = 0.0

  def execute(model: Model, numItems: Int): List[Double] = {

    val components = model.getComponents :+ Sink
    val start = components.head
    for (_ <- 0 until numItems)
      start.inputBuffer.enqueue(Item())

    var busy = {
      for (c <- components) yield (c, false)
    }.toMap

    val next = {
      for (i <- 0 until components.length - 1) yield (components(i), components(i + 1))
    }.toMap + (Sink -> null)

    def scheduleNextEvent(component: Component): Unit = {
      if (component.inputBuffer.isEmpty) {
        busy += (component -> false)
      } else if (busy(component)) {
        // do nothing
      } else {
        val item = component.inputBuffer.dequeue()
        component match {
          case o: Operation => doOperation(o, item)
          case t: Test => doTest(t, item)
          case Sink => doSink(item)
        }
      }
    }

    def doOperation(operation: Operation, item: Item): Unit = {
      busy += (operation -> true)
      schedule.enqueue(new Event(currentTime + operation.duration()) {
        override def fire(): Unit = {
          println(s"$currentTime: Item ${item.id} left operation ${operation.name}")

          next(operation).inputBuffer.enqueue(item)
          scheduleNextEvent(next(operation))

          busy += (operation -> false)
          scheduleNextEvent(operation)
        }
      })
    }

    def doTest(test: Test, item: Item): Unit = {
      print(s"$currentTime: Item ${item.id} is tested at test ${test.name} ...")
      if (test.isFailure()) {
        println("Failure")
        if (test.isScrap()) {
          // do nothing
          busy += (test -> false)
        } else {
          //rework
          busy += (test -> true)
          schedule.enqueue(new Event(currentTime + test.duration()) {
            println(s"$currentTime: Item ${item.id} was reworked at test ${test.name}")

            override def fire(): Unit = {
              test.inputBuffer.enqueue(item)
              busy += (test -> false)
            }
          })
        }
      } else {
        println("Success")
        schedule.enqueue(new Event(currentTime) {
          override def fire(): Unit = {
            println(s"$currentTime: Item ${item.id} left test ${test.name}")
            next(test).inputBuffer.enqueue(item)
            scheduleNextEvent(next(test))

            busy += (test -> false)
            scheduleNextEvent(test)
          }
        })
      }
    }

    def doSink(item: Item): Unit = {
      println(s"$currentTime: Item ${item.id} arrived in sink")
      Sink.times += currentTime
    }

    scheduleNextEvent(start)
    busy += (start -> true)

    while (schedule.nonEmpty) {
      val e = schedule.dequeue()
      currentTime = e.time
      e.fire()
    }

    Sink.times.toList
  }
}
