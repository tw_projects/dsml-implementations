package org.jamesii.sc.sim

/**
  * @author Tom Warnke
  */
abstract class Event(val time: Double) {
  def fire(): Unit
}


