package org.jamesii.sc.sim

/**
  * @author Tom Warnke
  */
class Item(val id: Int) {

  override def toString: String = s"Item($id)"

}

object Item {

  var idCounter = 0

  def apply(): Item = {
    idCounter = idCounter + 1
    new Item(idCounter)
  }
}
