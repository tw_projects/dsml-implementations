package org.jamesii.sc

/**
  * @author Tom Warnke
  */
class Model {

  private[this] var components = Seq[Component]()

  def getComponents = components

  def operation(name: String)(location: Double, shape: Double): Unit = {
    components = components :+ Operation(name, location, shape)
  }

  def test(name: String)(failureProb: Double) = ProtoTest(name, failureProb)



  case class ProtoTest(name: String, failureProb: Double) {
    def scrap(scrapProb: Double) = ProtoTest2(name, failureProb, scrapProb)
  }

  case class ProtoTest2(name: String, failureProb: Double, scrapProb: Double) {
    def rework(location: Double, shape: Double): Unit =
      components = components :+ Test(name, failureProb, scrapProb, location, shape)
  }

}