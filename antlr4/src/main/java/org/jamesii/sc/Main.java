package org.jamesii.sc;

import org.jamesii.sc.model.Model;
import org.jamesii.sc.sim.Simulator;

import java.io.IOException;
import java.util.List;

/**
 * @author Tom Warnke
 */
public class Main {

	public static void main(String[] args) throws IOException {
		new Main().doSimulations();
	}

	private void doSimulations() throws IOException {
		String modelFilePath = this.getClass().getClassLoader().getResource("testModel.scmodel").getPath();
		Model model = SupplyChainParserManager.parseModel(modelFilePath);

		Simulator simulator = new Simulator(model);
		List<Double> times = simulator.execute(100);

		Double average = 0.0;
		for (Double t: times) {
			average += t;
		}
		average /= times.size();

		System.out.println("\n\nAverage lead-time: " + average);

	}

}
