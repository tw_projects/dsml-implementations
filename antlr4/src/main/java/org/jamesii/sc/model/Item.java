package org.jamesii.sc.model;

/**
 * @author Tom Warnke
 */
public class Item {

	private final int id;

	private static int idCounter = 0;

	public Item() {
		id = idCounter++;
	}

	public int getId() {
		return id;
	}
}
