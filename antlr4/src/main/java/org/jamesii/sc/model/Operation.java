package org.jamesii.sc.model;

import org.apache.commons.math3.distribution.LogNormalDistribution;

/**
 * @author Tom Warnke
 */
public class Operation extends Component {

	private final String name;
	private final LogNormalDistribution durationDistribution;

	public Operation(String name, Double location, Double scale) {
		this.name = name;
		this.durationDistribution = new LogNormalDistribution(location, scale);
	}

	public String getName() {
		return name;
	}

	public Double duration() {
		return durationDistribution.sample();
	}
}
