package org.jamesii.sc.model;

import java.util.ArrayList;

/**
 * @author Tom Warnke
 */
public class Model {

	private final ArrayList<Component> components = new ArrayList<>();

	public boolean add(Component component) {
		return components.add(component);
	}

	public ArrayList<Component> getComponents() {
		return components;
	}
}
