package org.jamesii.sc.model;

import org.apache.commons.math3.distribution.LogNormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

/**
 * @author Tom Warnke
 */
public class Test extends Component {

	private final String name;
	private final UniformRealDistribution unif = new UniformRealDistribution();
	private final Double failureProbability;
	private final Double scrapProbability;
	private final LogNormalDistribution reworkDurationDistribution;

	public Test(String name, Double failureProbability, Double scrapProbability, Double location, Double scale) {
		this.name = name;
		this.failureProbability = failureProbability;
		this.scrapProbability = scrapProbability;
		this.reworkDurationDistribution = new LogNormalDistribution(location, scale);
	}

	public String getName() {
		return name;
	}

	public boolean isFailure() {
		return unif.sample() < failureProbability;
	}

	public boolean isScrap() {
		return unif.sample() < scrapProbability;
	}

	public Double getReworkDuration() {
		return reworkDurationDistribution.sample();
	}
}
