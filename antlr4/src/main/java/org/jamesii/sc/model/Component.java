package org.jamesii.sc.model;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * @author Tom Warnke
 */
public abstract class Component {

	private final Queue<Item> inputBuffer = new ArrayDeque<>();

	private boolean busy = false;

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public void enqueue(Item item) {
		inputBuffer.add(item);
	}

	public Item getNext() {
		return inputBuffer.remove();
	}

	public Item hasNext() {
		return inputBuffer.peek();
	}
}
