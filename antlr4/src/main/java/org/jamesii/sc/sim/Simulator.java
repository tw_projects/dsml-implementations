package org.jamesii.sc.sim;

import org.jamesii.sc.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * @author Tom Warnke
 */
public class Simulator {

	private Double currentTime = 0.0;

	private final PriorityQueue<Event> schedule = new PriorityQueue<>(Comparator.comparing(Event::getTime));

	private final ArrayList<Component> components;

	public Simulator(Model model) {
		components = model.getComponents();
		components.add(new Sink());
	}

	public List<Double> execute(Integer numItems) {

		// add items to queue of first step
		for (int i = 0; i < numItems; i++) {
			components.get(0).enqueue(new Item());
		}

		scheduleNextEvent(components.get(0));

		while (!schedule.isEmpty()) {
			Event event = schedule.remove();
			currentTime = event.getTime();
			event.fire();
		}

		Sink sink = (Sink) components.get(components.size() - 1);
		return sink.getTimes();
	}

	private void scheduleNextEvent(Component component) {
		if (component.isBusy()) {
			// do not schedule another event
			return;
		}
		if (component.hasNext() == null) {
			// do not schedule an event
			component.setBusy(false);
			return;
		}
		Item item = component.getNext();
		if (component instanceof Operation) {
			handleOperation((Operation) component, item);
			component.setBusy(true);
		} else if (component instanceof Test) {
			handleTest((Test) component, item);
		} else if (component instanceof Sink) {
			handleSink((Sink) component, item);
		}
	}

	private void handleOperation(Operation operation, Item item) {
		Double waitingTime = operation.duration();
		schedule.add(new Event(currentTime + waitingTime) {
			@Override
			public void fire() {
				System.out.println(currentTime + ": Item " + item.getId() + " left operation " + operation.getName());
				moveToNextComponent(item, operation);
			}
		});
	}

	private void handleTest(Test test, Item item) {
		System.out.print(currentTime + ": Item " + item.getId() + " is tested in " + test.getName() + " ... ");
		if (test.isFailure()) {
			System.out.println("Failure!");
			if (test.isScrap()) {
				// do nothing
				System.out.println(currentTime + ": Item " + item.getId() + " was scrapped in " + test.getName());
				test.setBusy(false);
				scheduleNextEvent(test);
			} else {
				// rework
				Double reworkTime = test.getReworkDuration();
				test.setBusy(true);
				schedule.add(new Event(currentTime + reworkTime) {
					@Override
					public void fire() {
						System.out.println(
								currentTime + ": Item " + item.getId() + " was reworked in test " + test.getName());
						test.enqueue(item);
						test.setBusy(false);
					}
				});
			}
		} else {
			System.out.println("Success!");
			test.setBusy(false);
			schedule.add(new Event(currentTime) {
				@Override
				public void fire() {
					moveToNextComponent(item, test);
				}
			});
		}
	}

	private void handleSink(Sink sink, Item item) {
		System.out.println(currentTime + ": Item " + item.getId() + " arrived in sink");
		sink.add(currentTime);
	}

	private void moveToNextComponent(Item item, Component component) {
		Component nextComponent = components.get(components.indexOf(component) + 1);

		// move finished item to next component
		nextComponent.enqueue(item);
		scheduleNextEvent(nextComponent);

		// start working on next item
		component.setBusy(false);
		scheduleNextEvent(component);
	}
}
