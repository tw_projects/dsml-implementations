package org.jamesii.sc.sim;

/**
 * @author Tom Warnke
 */
abstract class Event {

	private final Double time;

	Event(Double time) {
		this.time = time;
	}

	public Double getTime() {
		return time;
	}

	abstract public void fire();
}
