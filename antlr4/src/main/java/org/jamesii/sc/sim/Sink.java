package org.jamesii.sc.sim;

import org.jamesii.sc.model.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tom Warnke
 */
public class Sink extends Component {

	private final List<Double> times = new ArrayList<>();

	public void add(Double time) {
		times.add(time);
	}

	public List<Double> getTimes() {
		return times;
	}

}
