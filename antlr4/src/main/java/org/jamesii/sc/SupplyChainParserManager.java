package org.jamesii.sc;

import org.antlr.v4.runtime.*;
import org.jamesii.sc.model.Model;
import org.jamesii.sc.model.Operation;
import org.jamesii.sc.model.Test;

import java.io.IOException;

/**
 * @author Tom Warnke
 */
public class SupplyChainParserManager {

	private SupplyChainParserManager() {
		// no instantiation
	}

	public static Model parseModel(String modelFile) throws IOException {

		final Model model = new Model();

		SupplyChainLexer lexer = new SupplyChainLexer(new ANTLRFileStream(modelFile));
		SupplyChainParser parser = new SupplyChainParser(new CommonTokenStream(lexer));

		parser.addErrorListener(new BaseErrorListener() {
			@Override
			public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int
					charPositionInLine, String msg, RecognitionException e) {
				throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
			}
		});

		parser.addParseListener(new SupplyChainBaseListener() {
			@Override
			public void exitOperation(SupplyChainParser.OperationContext ctx) {

				String name = ctx.name().getText();
				Double location = Double.parseDouble(ctx.operationDuration().duration().location().FLOAT().getText());
				Double shape = Double.parseDouble(ctx.operationDuration().duration().shape().FLOAT().getText());

				model.add(new Operation(name, location, shape));
			}

			@Override
			public void exitTest(SupplyChainParser.TestContext ctx) {

				String name = ctx.name().getText();
				Double failureProb = Double.parseDouble(ctx.failureProb().prob().FLOAT().getText());
				Double scrapProb = Double.parseDouble(ctx.scrapProb().prob().FLOAT().getText());
				Double reworkDurationLocation = Double.parseDouble(ctx.reworkDuration().duration().location().FLOAT()
						.getText());
				Double reworkDurationShape = Double.parseDouble(ctx.reworkDuration().duration().shape().FLOAT()
						.getText());

				model.add(new Test(name, failureProb, scrapProb, reworkDurationLocation, reworkDurationShape));
			}
		});

		try {
			parser.model();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}

		return model;

	}
}
