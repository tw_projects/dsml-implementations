grammar SupplyChain;

model: (line LINEBREAK)+;

line: (operation | test);

operation: O COLON name operationDuration;
test: T COLON name failureProb SCRAP scrapProb REWORK reworkDuration;

name: STRING;
operationDuration: duration;
failureProb: prob;
reworkDuration: duration;
scrapProb: prob;
duration: LPAREN location COMMA shape RPAREN;
location: FLOAT;
shape: FLOAT;
prob: LPAREN FLOAT RPAREN;

O: 'O';
T: 'T';
REWORK: 'rework';
SCRAP: 'scrap';
COLON: ':';
STRING: ('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'-'|'0'..'9')*;
LINEBREAK: '\n';
LPAREN: '(';
COMMA: ',';
RPAREN: ')';
FLOAT: DIGIT+ (POINT DIGIT+)?;
DIGIT: ('0'..'9');
POINT: '.';
WHITESPACE : ( '\t' | ' ' | '\r' | '\u000C' )+ -> skip ;