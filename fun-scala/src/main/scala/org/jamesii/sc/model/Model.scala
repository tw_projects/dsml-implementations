package org.jamesii.sc.model

import org.apache.commons.math3.distribution.UniformRealDistribution

/**
  * @author Tom Warnke
  */
abstract class Model {

  val unif = new UniformRealDistribution()

  var steps = Seq.empty[Step]

  def addStep(step: Step) = {
    if (steps.nonEmpty) steps.last.next = Some(step)
    steps = steps :+ step
  }

  def operation(name: String)(location: Double, shape: Double): Step = {
    val o = new Operation(name, location, shape)
    addStep(o)
    o
  }

  def test(name: String)(failureProb: Double) = new {
    def scrap(scrapProb: Double) = new {
      def rework(location: Double, shape: Double): Test = {
        val t = new Test(name, failureProb, scrapProb, location, shape)
        addStep(t)
        t
      }
    }
  }

  def custom(name: String)(p: Item => Activity): Step = {
    val step = new Step(name) {
      override def process(item: Item): Activity = p(item)
    }
    addStep(step)
    step
  }

  def custom(step: Step): Step = {
    addStep(step)
    step
  }

}
