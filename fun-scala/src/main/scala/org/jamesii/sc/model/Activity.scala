package org.jamesii.sc.model

/**
  * @author Tom Warnke
  */
abstract class Activity {

  def duration: Double

  def finish(time: Double): Unit

  def unif(p: Double): Boolean = Step.unif(p)

}