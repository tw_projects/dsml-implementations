package org.jamesii.sc.model

/**
  * @author Tom Warnke
  */
class Item(val id: Int) {

  override def toString: String = s"Item($id)"

  def isBroken: Boolean = false

}

object Item {

  var idCounter = 0

  def apply(): Item = {
    idCounter = idCounter + 1
    new Item(idCounter)
  }
}
