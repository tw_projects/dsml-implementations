package org.jamesii.sc.model

import org.apache.commons.math3.distribution.{LogNormalDistribution, UniformRealDistribution}

import scala.collection.mutable

/**
  * @author Tom Warnke
  */
abstract class Step(val name: String) {

  var next: Option[Step] = None

  val queue: mutable.Queue[Item] = mutable.Queue.empty

  /** process an item, starting an activity */
  def process(item: Item): Activity

  override def toString: String = name

  def unif(p: Double): Boolean = Step.unif(p)
}

class Operation(name: String, location: Double, shape: Double) extends Step(name) {
  val durationDistribution = new LogNormalDistribution(location, shape)

  override def process(item: Item): Activity = new Activity {

    override def duration: Double = durationDistribution.sample()

    override def finish(time: Double): Unit = {
      next.foreach(_.queue.enqueue(item))
    }
  }
}

class Test(name: String, failureProb: Double, scrapProb: Double, location: Double, shape: Double) extends Step(name) {

  val reworkDurationDistribution = new LogNormalDistribution(location, shape)

  def isFailure(item: Item): Boolean = unif(failureProb)

  def isScrap(item: Item): Boolean = unif(scrapProb)

  def scrap(item: Item): Activity = new Activity {
    val duration: Double = 0.0
    override def finish(time: Double): Unit = {}
  }

  def rework(item: Item): Activity = new Activity {
    val duration: Double = reworkDurationDistribution.sample()
    override def finish(time: Double): Unit = queue.enqueue(item)
  }

  def onSuccess(item: Item): Activity = new Activity {
    val duration: Double = 0.0
    override def finish(time: Double): Unit = next.foreach(_.queue.enqueue(item))
  }

  override def process(item: Item): Activity =
    if (isFailure(item))
      if (isScrap(item)) scrap(item) else rework(item)
    else
      onSuccess(item)
}

object Step {
  val unifDistr = new UniformRealDistribution()
  def unif(p: Double): Boolean = unifDistr.sample() < p
}