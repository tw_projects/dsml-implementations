package org.jamesii.sc

import org.jamesii.sc.model.{Activity, Item, Model, Test}
import org.jamesii.sc.sim.Simulator

/**
  * @author Tom Warnke
  */
object Main extends App {

  val model = new Model {

    operation("op1")(0.4, 0.1)
    test("op1-test")(0.05) scrap (0.5) rework (0.1, 0.05)
    operation("op2")(0.4, 0.1)
    test("op2-test")(0.05) scrap (0.5) rework (0.1, 0.05)
    operation("op3")(0.4, 0.1)

    custom("bypass-t3") { item => new Activity {
      override def duration: Double = 0.0
      override def finish(time: Double): Unit =
        if (unif(0.25))
          t1.queue.enqueue(item)
        else
          o1.queue.enqueue(item)
      }
    }

    custom(new Test("op3-test", 0.05, 0.5, 0.1, 0.05) {
      override def isFailure(item: Item): Boolean = item.isBroken
    })



    val t1 = test("op3-test")(0.05) scrap (0.5) rework (0.1, 0.05)
    val o1 = operation("op4")(0.4, 0.1)
    test("op4-test")(0.05) scrap (0.5) rework (0.1, 0.05)
    operation("op5")(0.4, 0.1)
    val t2 = test("op5-test")(0.05) scrap (0.5) rework (0.1, 0.05)
    custom(new Test("op5-snd-test",  0.05, 0.5, 0.1, 0.05) {
      override def rework(item: Item): Activity = new Activity {
        override val duration: Double = reworkDurationDistribution.sample()
        override def finish(time: Double): Unit = t2.queue.enqueue(item)
      }
    })

    custom("bypass-test") { item => new Activity {
      override def duration: Double = 0.0
      override def finish(time: Double): Unit =
        if (unif(0.25))
          t1.queue.enqueue(item)
        else
          o1.queue.enqueue(item)
    }
    }

    val t3 = custom(new Test("item-attribute-test", 0.05, 0.5, 0.1, 0.05) {
      override def isFailure(item: Item): Boolean = item.isBroken
    })

    custom(new Test("snd-test",  0.05, 0.5, 0.1, 0.05) {
      override def rework(item: Item): Activity = new Activity {
        override val duration: Double = reworkDurationDistribution.sample()
        override def finish(time: Double): Unit = t3.queue.enqueue(item)
      }
    })
  }

  val times = Simulator.execute(model, 100)

  println(s"\n\nAverage lead-time: ${times.sum / times.size}")
}
