package org.jamesii.sc.sim

import org.jamesii.sc.model.{Activity, Item, Model, Step}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ListBuffer


/**
  * @author Tom Warnke
  */
object Simulator {

  def execute(model: Model, numItems: Int): Seq[Double] = {

    implicit val eventSorter = new Ordering[DoneEvent] {
      override def compare(x: DoneEvent, y: DoneEvent): Int = -x.fireTime.compareTo(y.fireTime)
    }

    type Schedule = mutable.PriorityQueue[DoneEvent]

    model.addStep(Sink)
    model.steps.head.queue.enqueue(List.tabulate(numItems)(_ => Item()): _*)

    val schedule = mutable.PriorityQueue.empty[DoneEvent]
    val busy = mutable.Map.empty[Step, Boolean]


    /** compute next state until schedule is empty (tail-recursive) */
    @tailrec def execute: Unit = {
      if (schedule.nonEmpty) {
        /** take first event from schedule and execute it */
        val event = schedule.dequeue()
        event.fire
        execute
      }
    }

    def scheduleForStep(step: Step, currentTime: Double): Unit = {
//    print(s"$currentTime: Scheduling for step ${step.name}: ")
      if (!busy.getOrElse(step, false))
        if (step.queue.nonEmpty) {
          // process next item
          val itemToProcess = step.queue.dequeue
          val event = new DoneEvent(currentTime, step, itemToProcess)
          busy.put(step, true)
          schedule.enqueue(event)
        } else {
          // no item available
//              println(s"aborted because no item in queue")
          busy.put(step, false)
        }
      else {
        // busy, do not schedule event
//            println(s"aborted because busy")
      }

    }

    class DoneEvent(creationTime: Double, step: Step, item: Item) {

      val activity: Activity = step.process(item)
      val fireTime: Double = creationTime + activity.duration

//      println(s"Event created for Item ${item.id} at time $fireTime")

      def fire: Unit = {

        println(s"$fireTime: Item ${item.id} finished at step ${step.name}")

        activity.finish(fireTime)

        busy.put(step, false)

        model.steps.foreach(scheduleForStep(_, fireTime))
      }
    }

    scheduleForStep(model.steps.head, 0)
    execute
    Sink.times
  }

  object Sink extends Step("sink") {
    val times: ListBuffer[Double] = mutable.ListBuffer.empty[Double]

    override def process(item: Item): Activity = new Activity {
      override def duration: Double = 0

      override def finish(time: Double): Unit = times += time
    }
  }

}
